# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

import json
import os
import random
import string

from typing import Any, Text, Dict, List, Optional

from datetime import datetime
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet



# class ActionHelloWorld(Action):

#     def name(self) -> Text:
#         return "action_hello_world"

#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

#         dispatcher.utter_message(text="Hello World!")

#         return []



class ActionReserverTable(Action):
    def name(self) -> Text:
        return "action_reserver_table"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:
        
        slot_nombre_personnes = tracker.get_slot("slot_nombre_personnes")
        slot_date_reservation = tracker.get_slot("slot_date_reservation")
        slot_nom_restaurant = tracker.get_slot("slot_nom_restaurant")
        
        if slot_nombre_personnes and slot_date_reservation and slot_nom_restaurant:
            disponibilite = True
            if disponibilite:
                numero_reservation = "123456"
                #dispatcher.utter_message(template="utter_reservation_confirmee", numero_reservation=numero_reservation)
            else:
                dispatcher.utter_message("Désolé, il n'y a pas de disponibilité pour cette réservation.")
        else:
            dispatcher.utter_message("Désolé, je n'ai pas pu traiter votre réservation. Veuillez fournir toutes les informations nécessaires.")
        
        return []


def generate_reservation_number():
    return ''.join(random.choices(string.digits, k=6))

class ActionSaveReservation(Action):

    def name(self) -> str:
        return "action_save_reservation"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: dict) -> list:
        numero_reservation = generate_reservation_number()

        new_reservation = {
            "reservation": {
                "numero_reservation": numero_reservation,
                "nom": tracker.get_slot("slot_nom"),
                "numéro de téléphone": tracker.get_slot("slot_numero_tel"),
                "nombre_personnes": tracker.get_slot("slot_nombre_personnes"),
                "date_reservation": tracker.get_slot("slot_date_reservation"),
                "commentaire": tracker.get_slot("slot_commentaire")
            }
        }

        reservations = []

        if os.path.exists("reservation.json"):
            with open("reservation.json", "r") as json_file:
                reservations = json.load(json_file)

        reservations.append(new_reservation)

        with open("reservation.json", "w") as json_file:
            json.dump(reservations, json_file, indent=4)

        dispatcher.utter_message(text=f"Votre réservation a été enregistrée avec succès. Votre numéro de réservation est {numero_reservation}.")

        return [SlotSet("slot_numero_reservation", numero_reservation)]


class ActionAnnulerReservation(Action):
    def name(self) -> Text:
        return "action_annuler_reservation"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:
    
        slot_numero_reservation = tracker.get_slot("slot_numero_reservation")
        if slot_numero_reservation:
            dispatcher.utter_message("Votre réservation avec le numéro {} a été annulée avec succès.".format(slot_numero_reservation))
        else:
            dispatcher.utter_message("Désolé, je ne trouve pas de réservation correspondante pour l'annuler.")
        
        return []


class ActionAfficherReservation(Action):

    def name(self) -> str:
        return "action_afficher_reservation"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: dict) -> list:
        numero_reservation = tracker.get_slot("slot_numero_reservation")

        if not numero_reservation:
            dispatcher.utter_message(text="Veuillez fournir votre numéro de réservation")
            return []

        if os.path.exists("reservation.json"):
            with open("reservation.json", "r") as json_file:
                try:
                    reservations = json.load(json_file)
                except json.JSONDecodeError:
                    reservations = []

            for reservation in reservations:
                dispatcher.utter_message(text=f"Voici les détails de votre réservation:\n"
                                                f"Numéro de réservation: {reservation['numero_reservation']}\n"
                                                f"Nom: {reservation['nom']}\n"
                                                f"Nombre de personnes: {reservation['nombre_personnes']}\n"
                                                f"Date de réservation: {reservation['date_reservation']}\n"
                                                f"Commentaire: {reservation.get('commentaire', 'Aucun')}")
                return []

        dispatcher.utter_message(text="Je n'ai trouvé aucune réservation avec ce numéro.")
        return []
